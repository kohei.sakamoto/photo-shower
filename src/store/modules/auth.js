import router from '@/router/index'
import createId from '@/util/uuid'
import firebase from 'firebase'

const ADMIN = 'ADMIN'
const GENERAL = 'GENERAL'

const Auth = {
  namespaced: true,
  state: {
    userName: '',
    userId: '',
    role: GENERAL,
    isAdminLoggedIn: false,
    isApproved: false,
    admin: {}
  },
  mutations: {
    setUserName(state, payload) {
      state.userName = payload
    },
    approve(state) {
      if (!state.isApproved) {
        state.isApproved = true
        state.userId = createId()
      }
      console.log('id', state.userId)
    },
    adminAuth(state, payload) {
      // 一応adminオブジェクト入れてるけど、リロードで消えちゃうので、あてにしない
      state.admin = payload
      state.isAdminLoggedIn = payload ? true : false
      state.role = payload ? ADMIN : GENERAL
      if (state.userName.length == 0) state.userName = 'admin'
      state.role = payload ? ADMIN : GENERAL
    }
  },
  actions: {
    onChange({ commit, state, rootState }, payload) {
      commit('cangeForm', payload)
    },
    submit({ commit, state, rootState }, payload) {
      commit('setUserName', payload, {})
      commit('approve', null, {})
      router.push('/top')
    },
    adminLogin({ commit, state, rootState }, payload) {
      const { email, password } = payload
      firebase
        .auth()
        .setPersistence(firebase.auth.Auth.Persistence.LOCAL)
        .then(function() {
          // Existing and future Auth states are now persisted in the current
          // session only. Closing the window would clear any existing state even
          // if a user forgets to sign out.
          // ...
          // New sign-in will be persisted with session persistence.
          return signIn()
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code
          var errorMessage = error.message
        })
      function signIn() {
        firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .then(function(promise) {
            commit('adminAuth', promise.user)
            router.push('admin')
          })
          .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code
            var errorMessage = error.message
            // ...
          })
      }
    },
    adminLogout({ commit }) {
      firebase
        .auth()
        .signOut()
        .then(function() {
          // Sign-out successful.
          commit('adminAuth', null)
        })
        .catch(function(error) {
          // An error happened.
        })
    }
  },
  getters: {
    userName: state => state.userName,
    userId: state => state.userId,
    isLoggedIn: state => state.isLoggedIn,
    isApproved: state => state.isApproved,
    isSoftLoggedIn: state => {
      const s = state.isApproved && state.userName.length != 0
      return s
    },
    isAdminLoggedIn: state => state.isAdminLoggedIn
  }
}

export default Auth
