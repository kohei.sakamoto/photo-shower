import firebase from 'firebase'
import { storageImageRef, dbPhotosRef, dbUsersRef, dbPickerRef } from '@/config'
import createId from '@/util/uuid'
import { dateFormat } from '@/util/date'
import { dirname } from 'path'

const INITIAL_STATE = () => ({
  photos: [],
  uploader: {
    input: {
      file: null,
      blob: ''
    },
    progress: null,
    error: null
  },
  picker: {
    list: [],
    current: null
  }
})

const Photos = {
  namespaced: true,
  state: INITIAL_STATE(),
  // actionで使うやつ、、、=> reducer的なやつ
  mutations: {
    setPhotos(state, payload) {
      let data = []
      if (payload != null) {
        data = Object.keys(payload).map(key => payload[key])
        // masonry側でやるべき？
        // data.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1))
      }
      data
      state.photos = data
    },
    setPicker(state, payload) {
      state.picker.current = payload.current || ''
      let data = payload.list || []
      console.log(data)
      state.picker.list = data
    },
    uploaderError(state, payload) {
      state.uploader.error = payload
    },
    setProgress(state, payload) {
      state.uploader.progress = payload
    },
    setUploaderInput(state, payload) {
      state.uploader.input = payload
    }
  },
  // eventで呼ぶやつ
  actions: {
    listenOnce({ commit }) {
      const photosRef = firebase.database().ref(dbPhotosRef)
      photosRef.once('value', snapshot => {
        commit('setPhotos', snapshot.val())
      })
    },
    listen({ commit }) {
      const photosRef = firebase.database().ref(dbPhotosRef)
      photosRef.on('value', snapshot => {
        commit('setPhotos', snapshot.val())
      })
    },
    listenPicker({ commit }) {
      const photosRef = firebase.database().ref(dbPickerRef)
      photosRef.on('value', snapshot => {
        commit('setPicker', snapshot.val())
      })
    },
    onChangeFile({ commit }, payload) {
      if (payload.type == 'reset') {
        commit('setUploaderInput', INITIAL_STATE().uploader.input)
      } else {
        commit('setUploaderInput', payload)
      }
    },
    upload({ commit, state }, payload) {
      const database = firebase.database()
      const storageRef = firebase.storage().ref()
      console.log('upload!!!!!!!!!!')
      const { input, userdata } = payload
      const { file, orientation = 0 } = input
      const { name } = file
      const { userId, userName } = userdata
      const createdAt = dateFormat(new Date(), 'YYYY-MM-DDThh:mm:ss')
      const fileName = `${createdAt}--${createId('xxxx')}--${name}`
      const uploadTask = storageRef.child(storageImageRef + fileName).put(file)
      uploadTask.on(
        'state_changed',
        snapshot => {
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          console.log('Upload is ' + progress + '% done')
          commit('setProgress', progress)
          switch (snapshot.state) {
            case 'paused':
              console.log('Upload is paused')
              break
            case 'runing':
              console.log('Upload is running')
              break
          }
        },
        error => {
          // エラーハンドリングはとりあえずなし！
          console.log('catch error!')
          commit('uploaderError', error)
        },
        () => {
          uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
            console.log('File available at', downloadURL)
            commit('setUploaderInput', INITIAL_STATE().uploader.input)
            commit('setProgress', INITIAL_STATE().uploader.progress)
            const fileDirName = fileName.replace(/\.|#|\$|\[|\]/g, '')
            database.ref(`${dbPhotosRef}/${fileDirName}`).set({
              path: downloadURL,
              name: fileDirName,
              active: true,
              order: 1,
              userId,
              userName,
              createdAt,
              orientation
            })
            // this.listenOnce()
          })
        }
      )
    },
    toggleItemActive({ commit, state }, payload) {
      const { name, checked } = payload
      let data = [],
        updates = {}
      data = state.photos.filter(s => s.name == name)
      if (data.length > 0) {
        updates = data[0]
      } else {
        console.log('nameがおかしい？')
        return
      }
      updates.active = checked
      firebase
        .database()
        .ref(`${dbPhotosRef}/${name}`)
        .update(updates) //promise返してる
    },
    pushPickup({ commit, state }, payload) {
      // 最後に入れる
      const data = state.picker.list
      // pickup内での被りは許さない
      if (data.filter(s => s == payload).length == 0) {
        data.push(payload)
        firebase
          .database()
          .ref(`${dbPickerRef}/list`)
          .set(data)
      }
    },
    unshiftPickup({ commit, state }, payload) {
      const data = state.picker.list
      // pickup内での被りは許さない & 元々いたら消して先頭に作り直す
      const newData = data.filter(s => s !== payload)
      newData.unshift(payload)
      firebase
        .database()
        .ref(`${dbPickerRef}/list`)
        .set(data)
    },
    updatePickup({ commit, state }, payload) {
      const current = state.picker.current
      const data = state.picker.list
      const next = data[0]
      // pickup内での被りは許さない
      const newData = data.filter(s => s !== current)
      if (data.length == 0) {
        // そもそもdataからならやる必要なし？
      } else {
        firebase
          .database()
          .ref(`${dbPickerRef}`)
          .set({
            current: next,
            list: newData
          })
      }
    }
  },
  // stateを渡すよう
  getters: {
    getPhotos: state => state.photos,
    getUploader: state => state.uploader
  }
}

export default Photos
