const INITIAL_STATE = {}

const Top = {
  namespaced: true,
  state: {
    modal: {
      type: '',
      content: {}
    }
  },
  // actionで使うやつ、、、=> reducer的なやつ
  mutations: {
    closeModal(state) {
      state.modal.type = ''
    },
    openLightBox(state, payload) {
      state.modal.type = 'lightBox'
      state.modal.content = {
        href: payload.href,
        index: payload.index,
        next: payload.next,
        prev: payload.prev
      }
    }
  },
  // eventで呼ぶやつ
  actions: {
    close({ commit }) {
      console.log('close modal!!!!!!')
      commit('closeModal')
    },
    openLightBox({ commit, state, rootState }, payload) {
      const index = Number(payload.index)
      const photos = rootState.Photos.photos
      const next = photos[index + 1] ? photos[index + 1].path : null
      const prev = photos[index - 1] ? photos[index - 1].path : null
      commit('openLightBox', { ...payload, next, prev })
    },
    changeLightBox({ commit, state, rootState }, payload) {
      const { index, path } = payload
      console.log(payload)
      const photos = rootState.Photos.photos
      const next = photos[index + 1] ? photos[index + 1].path : null
      const prev = photos[index - 1] ? photos[index - 1].path : null
      commit('openLightBox', { href: path, index, next, prev })
    }
  },
  // stateを渡すよう
  getters: {
    modal: state => state.modal
  }
}

export default Top
