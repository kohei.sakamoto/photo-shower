// import Auth from "./modules/Auth";

import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './modules/auth'
import Photos from './modules/photos'
import Top from './modules/top'
import * as Cookies from 'js-cookie'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const head = {
  state: {
    title: ['感想を入力', '確認画面', '送信完了']
  },
  mutations: {},
  actions: {},
  getters: {}
}

const initialState = {
  loading: false
}

export default new Vuex.Store({
  state: initialState,
  mutations: {
    updateLoading(state, loading) {
      state.loading = loading
    }
  },
  modules: {
    Auth,
    Photos,
    Top,
    head
  },
  plugins: [
    createPersistedState({
      paths: [
        'Auth.userName',
        'Auth.userId',
        'Auth.isApproved',
        'Auth.isAdminLoggedIn',
        'Auth.role'
      ],
      getState: key => Cookies.getJSON(key),
      setState: (key, state) =>
        Cookies.set(key, state, {
          expires: 7 //有効期限 i日間
          // secure: process.env.NODE_ENV === 'production'
        })
    })
  ]
})
