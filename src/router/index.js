import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
// pages
import Top from '@/pages/Top'
import About from '@/pages/About'
import Post from '@/pages/Post'
import Album from '@/pages/Album'
import Signup from '@/pages/Signup'
import Shower from '@/pages/Shower'
import AdminLogin from '@/pages/AdminLogin'
import Admin from '@/pages/Admin'

Vue.use(Router)

const routes = [
  {
    path: '/top',
    name: 'Top',
    component: Top,
    meta: { requiresAuth: true }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: { requiresAuth: true }
  },
  {
    path: '/post',
    name: 'Post',
    component: Post,
    meta: { requiresAuth: true }
  },
  {
    path: '/album',
    name: 'Album',
    component: Album,
    meta: { requiresAuth: true }
  },
  {
    path: '/shower',
    name: 'Shower',
    component: Shower
  },
  {
    path: '/',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/adminLogin',
    name: 'AdminLogin',
    component: AdminLogin
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin,
    meta: { requiresAdminAuth: true }
  }
]

let router = new Router({ routes })

router.beforeEach((to, from, next) => {
  const isSoftLoggedIn = store.getters['Auth/isSoftLoggedIn']
  if (to.matched.some(record => record.meta.requiresAuth) && !isSoftLoggedIn) {
    next({ path: '/', query: { redirect: to.fullPath } })
  } else {
    next()
  }
  const isAdminLoggedIn = store.getters['Auth/isAdminLoggedIn']
  console.log('isAdminLoggedIn', isAdminLoggedIn)
  if (to.matched.some(record => record.meta.requiresAdminAuth) && !isAdminLoggedIn) {
    next({ path: '/adminlogin', query: { redirect: to.fullPath } })
  } else {
    next()
  }
})

export default router
