// firebase config
export const firebase = {
  apiKey: 'AIzaSyCTjjCJM0gBqpi6Qy5NBT6cbMjlVLjDxlc',
  authDomain: 'skmtko-shower.firebaseapp.com',
  databaseURL: 'https://skmtko-shower.firebaseio.com',
  projectId: 'skmtko-shower',
  storageBucket: 'skmtko-shower.appspot.com',
  messagingSenderId: '1082788030692'
}

// firebase storage ref
// export const storageImageRef = 'images/'
export const storageImageRef = 'test/'

// firebase db ref
// export const dbPhotosRef = '/photos'
export const dbPhotosRef = '/testPhotos'
export const dbUsersRef = '/testUsers'
export const dbPickerRef = '/picker'
