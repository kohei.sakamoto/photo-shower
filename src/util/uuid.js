// お手軽で良さそう
export default function(format = 'xxxx-xxxx') {
  var d = +new Date()
  return format.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0
    return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
}
