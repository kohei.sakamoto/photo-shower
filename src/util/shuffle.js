// 重複なしでarrayをシャッフルする
// https://www.pandanoir.info/entry/2013/03/04/193704

export default function(arr = []) {
  const result = arr
  for (var i = arr.length - 1; i > 0; i = 0 | i - 1) {
    var j = 0 | Math.random() * (i + 1);
    var swap = arr[i];
    arr[i] = arr[j];
    arr[j] = swap;
  }
  return result
}